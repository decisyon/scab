# SCAB (Start Container At Boot) #
  
### Cosa è SCAB? ###

* Sistema per l'avvio di uno stack diDecisyon App Composer all'avvio del server
* Version 4.0

### Installazione ###

Installatre SCAB è abbastanza triviale, una volta soddisfatti i prerequisiti.


### Prerequisiti ###

- Docker presente nel sistema (e vabbè...)

-  [Portainer](https://www.portainer.io/documentation) deployato come container

- DAC Stack già deployato nel sistema (SCAB può avviare container ma **non** può crearne)

### Installazione e Configurazione ###

#### Installare SCAB su server ####

Per installare SCAB sul server è sufficiente scaricare il file **scab..sh** e copiarlo in una cartella del server con il metodo che più aggrada l'installatore

`#scp ./scab.sh root@docker.remote.server:/tmp`

`#ftp...`

`#winscp...`

`# and so on...`

Rendere eseguibile scab.sh:

`# chmod +x /tmp/scab.sh`

Eseguire lo script con il parametro "-setup" :

`# /tmp/scab.sh -setup`

Il setup copia i file necessari nel sistema, crea due cron nel crontab di root, uno per Portainer:

 - @reboot sleep 10 && docker run -d -p 9000:9000 --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/path/to/store/portainer/data portainer/portainer

e uno per SCAB:

 - @reboot sleep 30 && /opt/scab.sh

#### Configurare SCAB su server ####

I parametri per la configurazione sono presenti nel file **/ect/scab/config.file** (sforzo di immaginazione immane per trovare il nome del file)
Aprire il file con un editor di testo ad esempio **vi** o **vim**  impostare i valori corretti per:

- **ip_address**: 
Indirizzo ip del server (se non specificato, il sistema lo reupererà in automatico utilizzando il servizio ifconfig.me)
es: ip_address=192.168.1.125
es: ip_address=
- **portainer_admin_user**:
Username dell'utente amministrare di Portainer
- **portainer_admin_password**:
Password dell'utente amministrare di Portainer
- **log_path**:
Path del file di log
es: log_path=/var/log
- **log_file_name**:
Nome del file di log
es: log_file_name=automate_dac_starting.log
- **container_list**:
Lista separata da spazi dei container da avviare all'avvio del server
es: container_list=keycloak cornelius appcomposer gertrude flowable

#### Abilitare SCAB su server ####

L'installazione abilita l'avvio automatico dei container indicati all'avvio del server

#### Disabilitare SCAB su server ####

Per disattivare l'avvio automatico dello stack al boot del sistema è sufficiente editare il crontab di root e commentare o rimuovere la riga precedentemete inserita:

`# crontab -e`

`# @reboot sleep 30 && /opt/scab.sh`

## Who do I talk to? ##
* In caso di necessità mailto:fabio.pellizzaro@decisyon.com