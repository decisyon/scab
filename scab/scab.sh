#!/bin/bash
############################################################################################################
# Script per l'avvio automatico di uno stack di Decisyon AppComposer  
# 
# Avvia tutti i container riportati nella proprietà "container_list" del config.file
#
# Utilizza le api di Portainer, quindi richiede un'istanza di Portainer attiva sul server
# 
# INSTALLAZIONE:
# E' sufficiente copiare questo file sh in /opt, renderlo eseguibile, configurare il file "/etc/scab/config.file" 
# e nel caso sia necessario modificare utente e password per l'utente admin di portainer
#
# E' in grado di recuperare l'ip del server in caso la variabile "ip_address" non sia valorizzata
# questa funzionalità è utile ad esempio su aws, quando l'ip pubblico cambia ad ogni avvio del server stesso
#
# ATTENZIONE!!!! 
#LO STACK DEVE ESSERE GIA' PRESENTE NEL SERVER
# questo script può solo avviare container, non ne può creare di nuovi

###########################
#Variabili
config_file_path=/etc/scab
config_file_name=config.file
container_db_file=./container_list.db
###########################
#Funzioni

function get_timestamp () {
    local timestamp=$(date +"[%F]-%T")
    echo $timestamp
}

function wtl (){
    #Write To Log
    #$1 = log message
    #$2 = log file
    timestamp=$(get_timestamp)
    echo "$timestamp: $1" >> $2
}

function read_config () {
    ip_address=$(grep ip_address= $1)
    ip_address=${ip_address:11}
    portainer_admin_user=$(grep portainer_admin_user= $1)
    portainer_admin_user=${portainer_admin_user:21}
    portainer_admin_password=$(grep portainer_admin_password= $1)
    portainer_admin_password=${portainer_admin_password:25}
    log_path=$(grep log_path= $1)
    log_path=${log_path:9}
    log_file_name=$(grep log_file_name= $1)
    log_file_name=${log_file_name:14}
    container_list=$(grep container_list= $1)
    container_list=${container_list:15}
    log_file=$log_path/$log_file_name
}

function get_ip_address () {
    #TODO select ip address
    local internal_ip=$(hostname -I | awk '{print $1}')
    local public_ip=$(curl ifconfig.me)
    local ip_address=""
    if [ "internal_ip" = "public_ip" ]; then
        # server on datacenter
        ip_address=$public_ip
    elif [ "internal_ip" != "public_ip" ]; then
        # server on aws
        ip_address=$public_ip
    else
        echo "ERROR during ip configuration. EXIT"
        exit
    fi
    echo $ip_address
}

function get_token () {
    local IN=$(curl --location --request POST "http://$1:9000/api/auth" --header 'Content-Type: application/json' --data "{\"Username\": \"$2\",\"Password\": \"$3\"}" )
    IN=${IN:8:-2}
    echo $IN
}

function get_container_list () {
    #salva in un file id e nome del container per creare un mini dizionario
    docker ps -a --no-trunc |  awk '{print $1,$NF}' > $1
}


function get_container_id () {
    local container_id=""
    local id=$(grep $1 $container_db_file)
    #sottostringa per recuperare solo l'id
    if [ "$id" != "" ]; then
        container_id=${id:0:64}
    else 
        container_id="ERROR CONTAINER NOT FOUND"
    fi
    $(wtl "$1 container id: $container_id" $log_file)
    echo $container_id

}

function start_container () {
    #start container 
    #$1 = server ip address
    #$2 = container id
    #$3 = portainer auth token
    local exit code
    exit_code=$(curl --location --request POST "http://$1:9000/api/endpoints/1/docker/containers/$2/start" --header "Authorization: Bearer $3")
    status=$?
    #exit_code=$(curl --location --request POST "http://$ip_address:9000/api/endpoints/1/docker/containers/$appcomposer_container_id/start" --header "Authorization: Bearer $token")
    echo $exit_code
}

function napalm_on_old_portaner (){
    $(wtl "Rimuovo eventuali container di Portainer non utilizzari" $log_file)
    portainer_array=$(docker ps -a | grep portainer/portainer | grep -v Up | awk '{print $1}')
    if [ -z "$portainer_array"];
    then
        $(wtl "Nessun Container da rimuovere" $log_file)
    else
        $(wtl "Container da rimuovere : $portainer_array" $log_file)
        $(wtl "RUN: docker rm -f $portainer_array" $log_file)
        clean=$(docker rm -f $portainer_array)
        $(wtl "Container rimossi: $clean" $log_file)
        $(wtl "Fine cleanup..." $log_file)
    fi
}

function setup () {
    local log_file=./scab_setup.log
    user=$(whoami)
    if [ "$user" != "root" ];then
        echo "You MUST be root to install SCAB"
        $(wtl "#########################" $log_file)
        $(wtl "You MUST be root to install SCAB" $log_file)
        $(wtl "" $log_file)
        $(wtl "exit" $log_file)
        $(wtl "#########################" $log_file)
    else
        $(wtl "#########################" $log_file)
        $(wtl "Inizio procedura di installazione " $log_file)   
        #copio lo script nella derectory /opt/scab
        $(wtl "Creo la cartella di installazione" $log_file)
        $(wtl "mkdir -p /opt/scab" $log_file)
        mkdir -p /opt/scab
        $(wtl "Copio il file nella cartella" $log_file)
        $(wtl "cp \"$(readlink -f $0)\" \"/opt/scab\"" $log_file)        
        cp "$(readlink -f $0)" "/opt/scab"
        $(wtl "Cambio i permessi al file" $log_file)
        $(wtl "chmod +x /opt/scab/$0" $log_file)        
        chmod +x /opt/scab/$0
        #creo il file di configurazione
        $(wtl "Creo la cartella per il config.file" $log_file)
        $(wtl "mkdir -p $config_file_path" $log_file)
        mkdir -p $config_file_path
        config_file=$config_file_path/$config_file_name
        $(wtl "Creo il config.file => $config_file" $log_file)
        cat <<EOF  > $config_file_path/$config_file_name
ip_address=
portainer_admin_user=
portainer_admin_password=
log_path=/var/log
log_file_name=automate_dac_starting.log
container_list=keycloak cornelius appcomposer gertrude flowable
EOF
        $(wtl "Cambio i privilegi al config.file" $log_file)
        $(wtl "chmod 664 $config_file_path/$config_file_name" $log_file)
        chmod 664 $config_file_path/$config_file_name
    fi
    $(wtl "Inserisco il cron per Portainer nel crontab di root" $log_file)
    $(wtl "@reboot sleep 10 && docker run -d -p 9000:9000 --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce" $log_file)
    (crontab -l 2>/dev/null; echo "@reboot sleep 10 && docker run -d -p 9000:9000 --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce") | crontab -
    $(wtl "Inserisco il cron per SCAB nel crontab di root" $log_file)
    $(wtl "@reboot sleep 30 && /root/start_appcomposer.sh" $log_file)       
    (crontab -l 2>/dev/null; echo "@reboot sleep 30 && /root/start_appcomposer.sh") | crontab -
   $(wtl "fine procedura di installazione " $log_file)  
   $(wtl "#########################" $log_file)
}


###########################
#Main

if [ "$1" == "-setup" ]; then
    echo "Installing scab on your system..."
    $(setup)
    echo "Done"
    echo "You can find the installation log $( dirname "$(readlink -f -- "$0")" )/scab_setup.log "
    sleep 2
    exit
fi

read_config $config_file

#Config check
$(wtl "Config check..." $log_file)
if [ "$portainer_admin_user" == "" ]; then
    $(wtl "Config error!" $log_file)
    $(wtl "=> portainer_admin_user: NOT CONFIGURED " $log_file)
    $(wtl "Exit" $log_file)
    exit
elif [ "$portainer_admin_password" == "" ]; then
    $(wtl "Config error!" $log_file)
    $(wtl "=> portainer_admin_password: NOT CONFIGURED " $log_file)
    $(wtl "Exit" $log_file)
    exit
elif [ "$container_list" == "" ]; then
    $(wtl "Config error!" $log_file)
    $(wtl "=> container_list: NOT CONFIGURED " $log_file)
    $(wtl "Exit" $log_file)
    exit
else
     $(wtl "Config check... DONE" $log_file)
fi

get_container_list  $container_db_file

if [ "$ip_address" == "" ]; then
    ip_address=$(get_ip_address)
fi

echo "ip address: $ip_address"
$(wtl "ip address: $ip_address" $log_file)
##get portainer auth token
$(wtl "provo ad ottenere un auth token da portainer" $log_file)
token=$(get_token $ip_address $portainer_admin_user $portainer_admin_password)
#
container_list_array=($container_list)
for container in "${container_list_array[@]}"
do
    echo $container
    container_id=$(get_container_id "$container" )
    echo $container_id
    exit_code=$(start_container $ip_address $container_id $token)
    status=$?
    if [ $status -eq 0 ]; then
        if [ -z "$exit_code" ]; then
            echo "servizio $container avviato"
            $(wtl "servizio $container avviato" $log_file)
         else 
            echo "servizio $container avviato"
            echo "exit code: $exit_code"
            $(wtl "servizio $container avviato" $log_file)
            $(wtl "exit code: $exit_code" $log_file)       
        fi
    elif [ $status -gt 0 ]; then
        if [ -n "$exit_code" ]; then
            echo "servizio $container NON avviato"
            echo "exit code: $exit_code"
            echo "exit status: $status"
            $(wtl "servizio $container NON avviato" $log_file)
            $(wtl "exit code: $exit_code" $log_file)
            $(wtl "exit status: $status" $log_file)
        fi 
    fi
done

$(napalm_on_old_portaner)
$(wtl "..." $log_file)
$(wtl "That's all Folks!" $log_file)
$(wtl "              ...bye" $log_file)